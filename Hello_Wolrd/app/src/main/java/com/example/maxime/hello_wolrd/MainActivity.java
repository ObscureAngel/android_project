package com.example.maxime.hello_wolrd;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    int cpt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView texteInfo = findViewById(R.id.monTexte);
        texteInfo.setText(" HELLO! ");

        Button mBouton = findViewById(R.id.mBouton);
        mBouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                texteInfo.setText("-> " + (cpt++));

            }
        });
    }
}
