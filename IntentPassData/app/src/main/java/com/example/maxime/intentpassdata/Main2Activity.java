package com.example.maxime.intentpassdata;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView info;
    TextView info2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        info = findViewById(R.id.textView);
        info2 = findViewById(R.id.textView2);

        Bundle objectBundle = this.getIntent().getExtras();
        String InfoPasse= objectBundle.getString("passInfo");
        info.setText(InfoPasse);

        Intent defineIntent = new Intent(this, Main2Activity.class);
        objectBundle.putString("refresh",info2.getText().toString()); // on passe notre objet a notre activities
        defineIntent.putExtras(objectBundle );

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Main2Activity.super.onStop();
            }
        });
    }

}
