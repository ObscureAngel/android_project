package com.example.maxime.gessnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageButton img0;
    ImageButton img1;
    ImageButton img2;
    ImageButton img3;
    ImageButton img4;
    ImageButton img5;
    ImageButton img6;
    ImageButton img7;
    ImageButton img8;
    ImageButton img9;

    TextView resultat;
    TextView strValeurTest;

    Button test;

    private int valeurtest = 0;
    public int testValue = 0;
    private int maximum = 10;
    private int secret = (int)(Math.random() * maximum);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img0 = findViewById(R.id.imageButton10);
        img1 = findViewById(R.id.imageButton11);
        img2 = findViewById(R.id.imageButton12);
        img3 = findViewById(R.id.imageButton13);
        img4 = findViewById(R.id.imageButton14);
        img5 = findViewById(R.id.imageButton15);
        img6 = findViewById(R.id.imageButton16);
        img7 = findViewById(R.id.imageButton17);
        img8 = findViewById(R.id.imageButton18);
        img9 = findViewById(R.id.imageButton19);

        test = findViewById(R.id.buttonTest);

        resultat = findViewById(R.id.result);
        strValeurTest = findViewById(R.id.numberTest);

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(0);
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(1);
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(2);
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(3);
            }
        });

        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(4);
            }
        });

        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(5);
            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(6);
            }
        });

        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(7);
            }
        });

        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(8);
            }
        });

        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afficheTest(9);
            }
        });

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testValeur();
            }
        });
    }

    public void afficheTest(int value) {
        strValeurTest.setText(String.valueOf(value));
        valeurtest = value;
    }

    public void testValeur() {
        if (valeurtest == secret) {
            resultat.setText("Bravo ! C'était " + String.valueOf(secret) + ".");
            secret = (int)(Math.random() * maximum);
        } else if (valeurtest < secret) {
            resultat.setText("C'est plus !");
        } else if (valeurtest > secret) {
            resultat.setText("C'est moins !");
        }

        strValeurTest.setText("");
    }
}
