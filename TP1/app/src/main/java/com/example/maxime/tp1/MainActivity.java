package com.example.maxime.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button plusButton;
    Button lessButton;
    Button buttonDiv;
    Button buttonMul;
    Button buttonC;
    Button equalButton;
    Button pointButton;
    EditText digitScreen;

    private double firstDigit;
    private boolean operatorClick = false;
    private boolean update = false;
    private String operator = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        pointButton = (Button) findViewById(R.id.pointButton);
        plusButton = (Button) findViewById(R.id.plusButton);
        lessButton = (Button) findViewById(R.id.lessButton);
        buttonDiv = (Button) findViewById(R.id.buttonDivision);
        buttonMul = (Button) findViewById(R.id.buttonMultiplier);
        buttonC = (Button) findViewById(R.id.buttonC);
        equalButton = (Button) findViewById(R.id.equalButton);

        digitScreen = (EditText) findViewById(R.id.EditText01);

        plusButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                plusClick();
            }
        });

        lessButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moinsClick();
            }
        });

        buttonDiv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                divClick();
            }
        });

        buttonMul.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mulClick();
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                resetClick();
            }
        });

        equalButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                egalClick();
            }
        });

        pointButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick(".");
            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("0");
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("1");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("2");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("3");
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("4");
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("5");
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("6");
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("7");
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("8");
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chiffreClick("9");
            }
        });

    }

    public void chiffreClick(String str) {
        if (update) {
            update = false;
        } else {
            if(!digitScreen.getText().equals("0"))
                str = digitScreen.getText() + str;
        }
        digitScreen.setText(str);
    }

    public void plusClick() {

        if (operatorClick) {
            calcul();
            digitScreen.setText(String.valueOf(firstDigit));
        } else {
            firstDigit = Double.valueOf(digitScreen.getText().toString()).doubleValue();
            operatorClick = true;
        }
        operator = "+";
        update = true;
    }

    public void moinsClick() {
        if (operatorClick) {
            calcul();
            digitScreen.setText(String.valueOf(firstDigit));
        } else{
            firstDigit = Double.valueOf(digitScreen.getText().toString()).doubleValue();
            operatorClick = true;
        }
        operator = "-";
        update = true;
    }

    public void mulClick() {
        if (operatorClick) {
            calcul();
            digitScreen.setText(String.valueOf(firstDigit));
        } else {
            firstDigit = Double.valueOf(digitScreen.getText().toString()).doubleValue();
            operatorClick = true;
        }
        operator = "*";
        update = true;
    }

    public void divClick() {
        if (operatorClick) {
            calcul();
            digitScreen.setText(String.valueOf(firstDigit));
        } else {
            firstDigit = Double.valueOf(digitScreen.getText().toString()).doubleValue();
            operatorClick = true;
        }
        operator = "/";
        update = true;
    }

    public void egalClick() {
        calcul();
        update = true;
        operatorClick = false;
    }

    public void resetClick() {
        operatorClick = false;
        update = true;
        firstDigit = 0;
        operator = "";
        digitScreen.setText("");
    }

    private void calcul() {
        if (operator.equals("+")) {
            firstDigit = firstDigit + Double.valueOf(digitScreen.getText().toString()).doubleValue();
            digitScreen.setText(String.valueOf(firstDigit));
        }

        if (operator.equals("-")) {
            firstDigit = firstDigit - Double.valueOf(digitScreen.getText().toString()).doubleValue();
            digitScreen.setText(String.valueOf(firstDigit));
        }

        if (operator.equals("*")) {
            firstDigit = firstDigit * Double.valueOf(digitScreen.getText().toString()).doubleValue();
            digitScreen.setText(String.valueOf(firstDigit));
        }

        if (operator.equals("/")) {
            try {
                firstDigit = firstDigit / Double.valueOf(digitScreen.getText().toString()).doubleValue();
                digitScreen.setText(String.valueOf(firstDigit));
            } catch(ArithmeticException e) {
                digitScreen.setText("0");
            }
        }
    }
}
