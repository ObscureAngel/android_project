package com.example.maxime.drawinandroid;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Cercle {
    public int xc;
    public int yc;
    public int rayon;
    private Paint paint;

    public Cercle (int x, int y, int r) {
        this.xc = x;
        this.yc = y;
        this.rayon = r;
        paint = new Paint();
        paint.setColor(Color.rgb((int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)));
    }

    public void draw (Canvas canvas) {
        canvas.drawCircle(xc, yc, rayon, paint);
    }
}
