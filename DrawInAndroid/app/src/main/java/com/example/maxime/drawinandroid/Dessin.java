package com.example.maxime.drawinandroid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.LinkedList;

public class Dessin extends View implements View.OnTouchListener{

    private LinkedList<Cercle> cercles;

    public Dessin(Context context, AttributeSet attrs) {
        super(context, attrs);
        cercles = new LinkedList<Cercle>();
        this.setOnTouchListener(this);
    }

    @Override
    public void onDraw (Canvas canvas) {
        for (Cercle cercle: cercles) cercle.draw(canvas);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN :
                cercles.add(new Cercle(x, y , 40));
                break;
            case MotionEvent.ACTION_MOVE :
                Cercle clast = cercles.getLast();
                clast.rayon = (int)Math.sqrt((x-clast.xc)*(x-clast.xc) + (y-clast.yc)*(y-clast.yc));
                break;
        }

        this.invalidate();
        return true;
    }
}
